600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	        acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		                aowen10    	aowen10@jhu.edu       704 661 8270
Isaac Nemzer		        inemzer		inemzer@jhu.edu		310 779 0039

***********OUR MAIN TEST FILES ARE RUN USING "Make Test", not SkipBo.cpp**************************************************

We have implemented a very basic GUI. The implementation is in the Group3-7GUI folder. This is the only extra credit we have implemented.

Still Need To Do:
- put variables in initializer list in Game.cpp
- use a macro for skip-bo 13
- change name of isDone()
- change gitBegin to getInitial
- change getEnd to getFinal
- finish and test Smart AI
- better GUI
- undo/redo moves
- create action vector called History
- record how we're shuffling cards (so we can archive them in a move)

