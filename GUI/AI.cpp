/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include "AI.hpp"

#include <vector>
#include <iostream>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <string>
#include <chrono>
#include <thread>

using std::vector;
using std::cout;
using std::endl;

AI::~AI() {
	//clear HandPile piles
	vector<Pile>::iterator it0;
	for (it0 = HandPile.begin(); it0 < HandPile.end(); it0++) {
		it0->~Pile();
	}

	StockPile.~Pile();
			
	//clear discard piles
	vector<Pile>::iterator it;
	for (it = DiscardPiles.begin(); it < DiscardPiles.end(); it++) {
		it->~Pile();
	}
}

Action AI::getAction(Game &G) {
	cout << G.playerNames.at(G.currentPlayer) << " (AI) is making their move..." << endl;
	//delay AI
	std::this_thread::sleep_for(std::chrono::milliseconds(10));

	//randomly generate begin position that is between 5 and 14 (inclusive)
	int begin = generateBegin();

	//randomly generate end position that is between 1 and 4 (inclusive) or 10 and 14 (inclusive)
	int end = generateEnd();

	Action action(this->getPlayerID(), begin, end);

	//make sure action is valid
	while ((G.isValid(action) == false) || (action.checkMovement() == false)) {
		action.setBegin(generateBegin());
		action.setEnd(generateEnd());
	}

	cout << "AI is moving from " << action.getBegin() << " to " << action.getEnd() << endl;

	return action;
}

int AI::generateBegin() {
	struct timespec ts;
    	clock_gettime(CLOCK_MONOTONIC, &ts);

    	/* using nano-seconds instead of seconds */
    	srand((time_t)ts.tv_nsec);

	return (rand() % 10) + 5;
}

int AI::generateEnd() {
	struct timespec ts;
    	clock_gettime(CLOCK_MONOTONIC, &ts);

    	/* using nano-seconds instead of seconds */
    	srand((time_t)ts.tv_nsec);

	int temp = rand() % 14 + 1;

	while (temp > 4 && temp < 10) {
		srand(time(NULL));
		temp = (rand() % 14) + 1;
	}

	return temp;
}
