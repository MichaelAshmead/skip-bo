600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	        acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		                aowen10    	aowen10@jhu.edu       704 661 8270
Isaac Nemzer		        inemzer		inemzer@jhu.edu		310 779 0039

This is a very basic GUI. There is a single input box at the bottom of the webpage. It is basically just printing the TUI to a webpage. This GUI doesn't contain a working AI or the ability to load a save game. It was built from an older version of the main TUI files.

To add GUI files to ugrad servers:
0) delete GUI files that are already there
1) scp -r Group3-7GUI mashmea1@ugrad13.cs.jhu.edu:Documents

To run GUI program (uses bash script):
1) ./launch

To play game after loading GUI above:
1) Go to ugrad13.cs.jhu.edu:20007

To terminate GUI in command line:
1) ps -x
2) kill -9 <monitor.py task ID #>
3) kill -9 <server.py task ID #>
4) kill any other tasks that are a result of running program