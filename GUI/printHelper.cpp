/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using std::ostringstream;
using std::vector;

string otherPlayerToString(vector<int> v);
string buildingToString(vector<int> v);
string curPlayerToString(vector<int> v);


string createStr(vector <string> nameVect, vector <int> v){
	int place = 2;
	int cur = v.at(1);
	int players = v.at(0);
	unsigned long size = 0;
	string underBuilding = "            1    2    3    4\n";
	std::ostringstream oss;

	oss << "<pre>";

	for (int i = 0; i < players; i++) {
		if (i!=(cur)) {
			if (size<nameVect.at(i).length())
				size=nameVect.at(i).length();
			vector<int> otherV(6);
			for (int j = 0; j < 6; j++) {
				otherV.at(j) = v.at(place);
				place++;
			}
			oss << nameVect.at(i) << ":  ";
			oss << otherPlayerToString(otherV);
		}
	}
	for (int i = 0; i < (int)(size+ 2)/3 + 1; i++) {
		oss << "-- ";
	}
	oss << "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- " << endl;
	vector<int> bldg(4);
	for (int i = 0; i < 4; i++){
		bldg.at(i) = v.at(place);
		place++;
	}
	oss << buildingToString(bldg);
	oss << underBuilding << endl;
	vector<int> curPlayer(11);
	for (int i = 0; i < 11; i++){
		curPlayer.at(i) = v.at(place);
		place++;
	}

	oss << nameVect.at(cur) << ": ";
	oss << curPlayerToString(curPlayer);
	size = nameVect.at(cur).length();
	for (int i = 0; i < (int) size; i++) {
		oss <<' ';
	}
	oss << "          5    6    7    8    9             10   11   12   13           14\n";

	oss << "</pre>";

	return oss.str();
}

string otherPlayerToString(vector<int> v){
	ostringstream oss;
	oss << "Discard: ";
	for (int i = 0; i < 5; i++) {
		if (i == 4)
			oss << "  Stock: ";
		if(v.at(i) == 0)
			oss << "[  ] ";
		else if(v.at(i) == 13)
			oss << "[ *] ";
		else if(v.at(i)<10)
			oss << "[ " << v.at(i) << "] ";
		else
			oss << '[' << v.at(i) << "] ";
		
	}
		
	oss << "Stock Remaining: ";
	oss << v.at(5);
	oss << endl;
	return oss.str();
}

string buildingToString(vector<int> v){
	ostringstream oss;
	oss << "Building:";
	for (int i = 0; i < 4; i++) {
		if (v.at(i) < 1)
			oss << " [  ]";
		else if (v.at(i) < 10)
			oss << " [ "<< v.at(i) << ']';
		else if (v.at(i) > 9)
			oss << " [" << v.at(i) << ']';
	}
	oss << endl;
	return oss.str();
}
string curPlayerToString(vector<int> v){
	ostringstream oss;
	oss << "Hand: ";
	for (int i = 0; i < 10; i++) {
		if (i == 5)
			oss << " Discard: ";
		if (i == 9)
			oss << " Stock: ";
		if (v.at(i) == 0)
			oss << "[  ] ";
		else if (v.at(i)== 13)
			oss << "[ *] ";
		else if (v.at(i)<10)
			oss << "[ " << v.at(i )<< "] ";
		else
			oss << '[' << v.at(i) << "] ";	
	}
	oss << " Stock Remaining: ";
	oss << v.at(10 )<< endl;
	
	return oss.str();
}
