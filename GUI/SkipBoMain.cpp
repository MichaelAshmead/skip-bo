/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include "Game.hpp"

#include <vector>
#include <string>
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <ctime>
#include <algorithm>

using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::vector;

int main(void) {
	int gameChoice = 0; //will the game be a new game or a loaded game from a file
	int numOfPlayers;
	vector<string> playerNames;
	vector<int> playerTypes;
	std::ofstream out;

	//get from the user whether or not we should initialize a new game or load a saved game
	try {
		out.open("templates/out.html");
		out << "Start a new game (1) or load a saved game (2)?" << endl;
		out.close();
		cin >> gameChoice;
		cout << gameChoice << endl;
		if (gameChoice != 1 && gameChoice != 2)
			throw 0;
	}
	catch (int e) {
		out.open("templates/out.html");
		out << "You didn't enter a valid game initialization!" << endl;
		out.close();
		exit(EXIT_FAILURE);
	}

	if (gameChoice == 1) {
		//get how many players there will be
		try {
			out.open("templates/out.html");
			out << "How many players will play? (2-6)" << endl;
			out.close();
			cin >> numOfPlayers;
			cout << numOfPlayers << endl;
			out.open("templates/out.html");
			out.close();
			if (numOfPlayers < 2 || numOfPlayers > 6) {
				out.open("templates/out.html");
				out.close();
				throw 0;
			}
		}
		catch (int e) {
			out.open("templates/out.html");
			out << "You didn't enter a valid number of players!" << endl;
			out.close();
			exit(EXIT_FAILURE);
		}

		//get the names of each player (for both AIs and Persons)
		string playername;
		for (int i = 1; i <= numOfPlayers; i++) {
			out.open("templates/out.html");
			out << "Please enter the name of player " << i << ':' << endl;
			out.close();
			cin >> playername;
			cout << playername << endl;
			playerNames.push_back(playername);
		}

		//get the types of each player
		int type;
		for (int i = 1; i <= numOfPlayers; i++) {
			try {
				out.open("templates/out.html");
				out << "Please enter whether player " << i << " is a person (0) or AI (1)?" << endl;
				out.close();
				cin >> type;
				cout << type << endl;
				if (type != 0 && type != 1)
					throw 0;
				else
					playerTypes.push_back(type);
			}
			catch (int e) {
				out.open("templates/out.html");
				out << "You didn't enter a valid player type!" << endl;
				out.close();
				exit(EXIT_FAILURE);
			}
		}
		
		//set stockdepth
		int stockDepth;
		try {
			out.open("templates/out.html");
			out << "Please enter the stock pile size that each player will have:" << endl;
			out.close();
			cin >> stockDepth;
			cout << stockDepth << endl;
			if (stockDepth < 1 || stockDepth > 30)
				throw 0;
		}
		catch (int e) {
			if (numOfPlayers <= 4) 
				stockDepth = 30;
			else 
				stockDepth = 20;
			
			out.open("templates/out.html");
			out << "That is not a valid stock pile size. The default size of " << stockDepth << " has been defaulted to." << endl;
			out.close();
		}

		//initialize game
		//PlayerNames is a vector<string> of the player names
		Game G(numOfPlayers, playerNames, playerTypes, stockDepth);

		//play the game now
		G.playGame();
	}

	return 0;
}
