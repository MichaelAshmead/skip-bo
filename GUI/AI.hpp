/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#ifndef _AI_H
#define _AI_H

#include "Game.hpp"
#include "Player.hpp"
#include "Action.hpp"
#include <vector>
#include <iterator>
#include <cstdlib>
#include <ctime>
#include <string>

using std::vector;

class AI : public Player {
 	public:
		AI(int id, string na) : Player(id, na, vector<Pile>(), Pile(30), vector<Pile>()) {
			for (int x = 0; x < 4; x++) {
				DiscardPiles.push_back(Pile(162));
			}
			for (int x = 0; x < 5; x++) {
				HandPile.push_back(Pile(2));
			}
		};

		~AI();

		Action getAction(Game& G);

		int generateBegin();

		int generateEnd();

		vector<int> cardsToAvoid(Game &G);

		bool canPlayStock(Game &G);

		int prefDiscard();

		bool behind(Game &G);
}; 

#endif
