/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#ifndef _GAME_H
#define _GAME_H

#include "Card.hpp"
#include "Pile.hpp"
#include "Action.hpp"

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

using std::vector;
using std::cout;
using std::cin;
using std::endl;
using std::string;

//forward declarations
class Player;
class Person;
class AI;

class Game {
	private:
		int numberPlayers;
		int stockPileSize;
		vector<Player*> players;
		vector<Action> histOfActions;
		int playerWon = -1; //game keeps going until this is set to a valid playerID (the playerID is set to the player that won the game)

	public:
		int currentPlayer = 0; //int ID of who the current player is (made public so that AI can access)
		vector<string> playerNames; //made public so that AI can access
		Pile draw = Pile(162);
		Pile scrap = Pile(162);
		vector<Pile> buildingPiles;

		//constructor for a new game
		Game(int numberPlaying, vector<string> names, vector<int> typeOfPlaying, int stockSize, int random = 0);

		//destructor
		~Game() {};

		void playGame();

		bool isValid(Action a);

		void doAction(Action a);
		
		string displayBoard();

		//returns the value of the card being moved in a Action
		int getMovingCardID(Action a);

		void undo(); //only prints the menu once all the moves have been removed.

		void redo(); //only prints the menu once all the moves have been redone.
};

#include "Player.hpp"
#include "Person.hpp"
#include "AI.hpp"

#endif
