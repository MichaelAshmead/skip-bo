/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#ifndef _Player_H
#define _Player_H

#include "Pile.hpp"
#include "Action.hpp"
#include <string>
#include <vector>

using std::vector;
using std::string;

//abstract base class
//will be overridden by Person class and AI class
class Player {
	public: 
		int playerID;
		string name;
		vector<Pile> HandPile;
		Pile StockPile;
		vector<Pile> DiscardPiles;

		Player(int id, string na, vector<Pile> hand, Pile stock, vector<Pile> dis) : playerID(id), name(na), HandPile(hand), StockPile(stock), DiscardPiles(dis) {};
		
		virtual ~Player() {};

		int getPlayerID() {
			return playerID;
		};

		string getName() {
			return name;
		};

		bool emptyHand(){
			for (vector<Pile>::iterator i = HandPile.begin(); i != HandPile.end(); i++){
				if (i->getBack().getCardID()!=0)
					return false;
			}
			return true;
		}

  		virtual Action getAction(Game & G) = 0; //pure virtual method that prevents this class from being instantiated
};

#endif

