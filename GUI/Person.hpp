/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#ifndef _Person_H
#define _Person_H

#include "Game.hpp"
#include "Player.hpp"
#include "Action.hpp"
#include <vector>
#include <string>
#include <iterator>
#include <fstream>
#include <iostream>

using std::vector;
using std::string;
using std::cin;
using std::cout;

class Person : public Player {
 	public:
  		Person(int id, string na) : Player(id, na, vector<Pile>(), Pile(30), vector<Pile>()) {
			for (int x = 0; x < 4; x++) {
				DiscardPiles.push_back(Pile(162));
			}
			for (int x = 0; x < 5; x++) {
				HandPile.push_back(Pile(2));
			}
		};

		~Person() {
			//clear HandPile piles
			vector<Pile>::iterator it0;
			for (it0 = HandPile.begin(); it0 < HandPile.end(); it0++) {
				it0->~Pile();
			}

			//clear StockPile
			StockPile.~Pile();
			
			//clear discard piles
			vector<Pile>::iterator it;
			for (it = DiscardPiles.begin(); it < DiscardPiles.end(); it++) {
				it->~Pile();
			}
		};

		Action getAction(Game &G) {			
			vector<int> beginAndEnd = promptAction(G);
			Action action(this->getPlayerID(), beginAndEnd.at(0), beginAndEnd.at(1));
			
			std::ofstream out;
			//make sure action is valid
			while (!G.isValid(action)) {
				out.open("templates/out.html");
				out << "Move is illegal. Please enter another move." << endl;
				out.close();
				beginAndEnd = promptAction(G);
				action.setBegin(beginAndEnd.at(0));
				action.setEnd(beginAndEnd.at(1));
			}

			return action;
		};

		//ask for user input
		vector<int> promptAction(Game &G) {
			int firstInput;

			//show current board to Person Player
			string board = G.displayBoard();

			std::ofstream out;
			out.open("templates/out.html");
			out << board << endl << "Begin? (Use 100 to Save):" << endl;
			out.close();
			cin >> firstInput;
			cout << firstInput << endl;

			int secondInput = 0;

			if (firstInput >= 2 && firstInput <= 14) {
				out.open("templates/out.html");
				out << board << endl << "End?:" << endl;
				out.close();
				cin >> secondInput;
				cout << secondInput << endl;
			}
			
			vector<int> beginAndEnd = vector<int>();
			beginAndEnd.push_back(firstInput);
			beginAndEnd.push_back(secondInput);
			
			return beginAndEnd;
		};
}; 

#endif
