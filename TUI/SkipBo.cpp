/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include "Card.hpp"
#include <cassert>
#include <iostream>
#include <string>
#include <vector>
#include "Game.hpp"
#include "Pile.hpp"


using std::cout;
using std::endl;
using std::vector;
using std::string;

class SkipBo {
	public:
		static void CardTest(){
			cout << "Testing Card Class..." << endl;
			Card c1(3);
   			Card c2(4);
   			Card c3(13);
  			Card c4(13);
 			Card c5(2); 
   			assert(!(c1==c5));
   			assert(!(c3==c5));
   			assert(c3==c4);
			Card c10(6);
			Card c6(2);
   			Card c7(6);
   			Card c8(10);
   			Card c9(13);
   			assert(c10==c7);
			cout << "Card Class Tests Passed" << endl;
		}

		static void PileTest() {
			//PILE 1 TEST
			cout << "Testing Pile Class..." << endl;
			Pile hand(5);
	   		hand.addCard(3);
   			hand.addCard(4);
   			hand.addCard(13);
   			hand.addCard(13);
   			hand.addCard(2);
   			assert(hand.getSize()==5);
   			assert(!(hand.addCard(6)));
   			assert(hand.removeCard(3)); 
   			assert(!(hand.removeCard(12)));  	 
  			assert(hand.removeCard(13)); 	 
   			assert(hand.removeCard(13));  
    			assert(hand.removeCard(4));
    			assert(hand.removeCard(2));
    			assert(!(hand.removeCard(5))); 
    			assert(hand.getSize()!=5);

 			//PILE 2 TEST
   			Pile discard1(4);
   			discard1.addCard(2);
   			discard1.addCard(6);
   			discard1.addCard(10);
   			discard1.addCard(13);
   			assert(discard1.getSize()==4);
   			Card c11(12);
   			assert(!(discard1.addCard(12)));
   			assert(discard1.removeCard(2));
   			assert(discard1.removeCard(6));
   			assert(discard1.removeCard(10));
   			assert(discard1.removeCard(13));
   			assert(!(discard1.removeCard(2)));
   			assert(!(discard1.removeCard(13)));
   			assert(discard1.getSize()!=4);
   			cout << "Pile Class Tests Passed" << endl;
		}

		static void GameTest(){
			//Game Tests
			cout << "Testing Game and Player Classes..." << endl;
    			vector<string> Names;
    			Names.push_back("Alex");
    			Names.push_back("Michael");
    			Names.push_back("Isaac");
    			Names.push_back("Andrew");
    			vector<int> Comp(4,0);
    			Game TestGame(4,Names,Comp,30);    
    			assert(TestGame.numberPlayers==4);    
    			assert(TestGame.stockPileSize==30); //checks that the stock piles are at 30   
    			assert(Names==TestGame.playerNames);  //Makes sure that the Name vector was initialized  in the Game
    			assert(TestGame.playerWon == 0);    //makes sure this value hasnt changed from the default
    			assert(TestGame.players.at(0)->name=="Alex");   //checks to make sure the names were initialized correctly for each player
    			assert(TestGame.players.at(1)->name=="Michael");
    			assert(TestGame.players.at(2)->name=="Isaac");
    			assert(TestGame.players.at(3)->name=="Andrew");
    			assert(TestGame.draw.getSize()==42);
    			for (int i = 0; i < 4; i++) { //loops throught the players
    				TestGame.currentPlayer=i;
    				for (int j = 0; j < 4; j++) {
    					assert(TestGame.players.at(i)->DiscardPiles.at(j).getSize()==0);
    				}
    				for (int j = 0; j < 5; j++) {
    					assert(TestGame.players.at(i)->HandPile.at(j).getSize()==0);
    				}
    				assert(TestGame.players.at(i)->emptyHand());//checks to see if the hands are empty as they should be at the beginning of the game
    				assert(TestGame.players.at(i)->getPlayerID()==i);// checks to see if the player ID is appropriate
    				assert(TestGame.players.at(i)->StockPile.getSize()==30);
    				for (int x = 0; x < 5; x++) {
    					assert(TestGame.players.at(i)->HandPile.at(x).getCard(0).getCardID()==0); //first card in Pile is O
					Action drawToHand = Action(TestGame.currentPlayer, 0, x + 5);
					TestGame.doAction(drawToHand);
					assert(!(TestGame.players.at(i)->emptyHand()));//the hand cant be empty now
					assert(TestGame.players.at(i)->HandPile.at(x).getSize()==1);
					assert(TestGame.players.at(i)->HandPile.at(x).getCard(1).getCardID()!=0); // Second card in pile can't be 0 because no 0 cards in deck
					assert(TestGame.players.at(i)->HandPile.at(x).getCard(0).getCardID()==0);//first card in Pile is still 0
				} //if this for loop has no errors, then it shows that the draw Card operation is succesful in some way
				assert(TestGame.draw.getSize()!=42);
				assert(TestGame.players.at(i)->StockPile.getSize()==30);//stock Piles should still be 30

				assert(!(TestGame.players.at(i)->emptyHand()));//makes sure the hand isnt empty
    			}
    
    			assert(TestGame.scrap.getSize()==0);
    			for (int i = 0; i < 4; ++i){
    				assert(TestGame.buildingPiles.at(i).getSize()==0);
    			}

    			cout << "Game and Player Classes Tests Passed" << endl;
		}

		/*static void AITest() {
			cout << "Testing AI Class..." << endl;

			//initialize a game to run AI tests
			vector<string> Names;
    			Names.push_back("Alex");
    			Names.push_back("Michael");
    			Names.push_back("Isaac");
    			Names.push_back("Andrew");
    			vector<int> Comp(4,0);

    			Game TestGame(4,Names,Comp,30);
			AI AI1(0, "AI1");
			Action action1(AI1.getAction(TestGame));
			assert(action1.getPlayerID() == 0);
			assert(action1.getBegin() > 0 && action1.getBegin() < 15);
			assert(action1.getEnd() > 0 && action1.getEnd() < 15);
	
			AI AI2(2, "AI1");
			Action action2(AI2.getAction(TestGame));
			assert(action2.getPlayerID() == 2);
			assert(action2.getBegin() > 0 && action2.getBegin() < 15);
			assert(action2.getEnd() > 0 && action2.getEnd() < 15);

			AI AI3(4, "AI1");
			Action action3(AI3.getAction(TestGame));
			assert(action3.getPlayerID() == 4);
			assert(action3.getBegin() > 0 && action3.getBegin() < 15);
			assert(action3.getEnd() > 0 && action3.getEnd() < 15);

			AI AI4(6, "AI1");
			Action action4(AI4.getAction(TestGame));
			assert(action4.getPlayerID() == 6);
			assert(action4.getBegin() > 0 && action4.getBegin() < 15);
			assert(action4.getEnd() > 0 && action4.getEnd() < 15);

			cout << "AI Class Passed All Tests!" << endl;
		}*/
	};
	int main(void) {
		SkipBo::CardTest();
		SkipBo::PileTest();
		SkipBo::AITest();
		SkipBo::GameTest();
	}
