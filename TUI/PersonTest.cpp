/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include "Person.hpp"
#include <cassert>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::vector;

int main(void) {
	Pile hand(5, 0);
	Card c1(13);
	Card c2(10);
	Card c3(9);
	Card c4(4);
	Card c5(5);
	hand.addCard(c1);
	hand.addCard(c2);
	hand.addCard(c3);
	hand.addCard(c4);
	hand.addCard(c5);

	Pile stock(5, 1);
	Card c6(13);
	Card c7(10);
	Card c8(9);
	Card c9(4);
	Card c10(5);
	stock.addCard(c6);
	stock.addCard(c7);
	stock.addCard(c8);
	stock.addCard(c9);
	stock.addCard(c10);

	Pile dis1(10);
	Card c11(1);
	Card c12(10);
	dis1.addCard(c11);
	dis1.addCard(c12);

	Pile dis2(10);
	Card c13(12);
	Card c14(11);
	dis2.addCard(c13);
	dis2.addCard(c14);

	Pile dis3(10);
	Card c15(8);
	Card c16(5);
	dis3.addCard(c15);
	dis3.addCard(c16);

	Pile dis4(10);
	Card c17(3);
	Card c18(6);
	dis4.addCard(c17);
	dis4.addCard(c18);

	vector<Pile> dis = {dis1, dis2, dis3, dis4};
	Person p1(1, "Andrew", hand, stock, dis);
}
