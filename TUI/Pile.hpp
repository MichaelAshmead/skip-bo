/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#ifndef _PILE_H
#define _PILE_H

#include "Card.hpp"

#include <vector>
#include <iostream>
#include <algorithm> //used for shuffling deck of cards
#include <ctime>
#include <cstdlib>

using std::vector;  using std::cout;  using std::cerr; using std::endl;

class Pile {
	private:
		int max; //max cap of the cards of cards
		int size; //size of the cards of cards
		vector<Card> cards; //vector of cards in the pile

	public:
		Pile(int m = 0): max(m), size(0) {};
		
		//destructor
		~Pile() {
			vector<Card>::iterator it;
			for (it = cards.begin(); it < cards.end(); it++) {
				it->~Card();
			}
		}

  		bool removeCard(int value) {
  			if(size==0)
  				return false;
  			else{
			int count=-1;
			for (int i = 0; i < size; i++)
			{
				if(cards.at(i).getCardID()==value)
					count=i;
			}
			if (count==-1)
				return false;
			int iterator=0;
			for (vector<Card>::iterator iter = cards.begin(); iter != cards.end(); iter++){
    			if (iterator==count) {
      	  			cards.erase(iter);
					break;
    			}
    			else
    			iterator++;
			}
			size-=1;
			return true;
		    }
		};

  		bool addCard(int temp) {
			Card c(temp);
			if (size < max){
				size+=1;
				cards.push_back(c);
				return true;
			}
			else if (size==max){
				cerr << "Cannot add card, pile full!" << endl;
				return false;
			}
			else
				return false;
		};

		//returns the card at a location in the pile w/ 0 being the first location
		Card getCard(int loc) {
			return cards.at(loc);
		};

		//return the card at the very top of the pile
		Card getBack() {
			return cards.back();
		};

		int getRealSize(){
			return size;
		};
		//returns the size of the passed pile
  		int getSize() {
  			if (size==0)
  				return 0;
			if (cards.at(0).getCardID() == 0) { //pile is built on top of a 0 card
				return size - 1;
			}
			else {
				return size;
			}
		};
		void setLoc(int loc, int cardnum){
			Card newCard(cardnum);
			cards.at(loc)=newCard;
		};

		void shuffle() {
			srand(time(NULL));
			std::random_shuffle(cards.begin(), cards.end());
		};
};

#endif
