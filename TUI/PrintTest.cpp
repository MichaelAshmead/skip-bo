/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

//testPrint
#include <string>
#include <cstdio>
#include <sstream>
#include <cassert>
#include <iostream>
#include <vector>
#include <fstream>

using std::string;
using std::cout;
using std::endl;
using std::ostringstream;
using std::ifstream;

void testOtherPlayerToString();
string otherPlayerToString(std::vector <int> v);
string buildingToString(std::vector<int> v);
void testBuildingToString();
string curPlayerToString(std::vector<int> v);
void testCurPlayerToString();
void wholeTest();
string createStr(std:: vector <string> nameVect, std:: vector <int> v);

int main()
{
	cout<<"Testing Print Function"<< endl;
	testOtherPlayerToString();
	cout<< "otherPlayerToString Tests Passed."<<endl;
	testBuildingToString();
	cout<< "testBuildingToString Tests Passed."<<endl;
	testCurPlayerToString();
	cout<< "testCurPlayerToString Tests Passed"<<endl;
	wholeTest();
	
	return 0;

}
string otherPlayerToString(std::vector <int> v){
	ostringstream oss;
	oss<< "Discard: ";
	for (int i = 0; i <5; i++)
		{
		if(i==4)
			oss<< "  Stock: ";
		if(v.at(i)==0)
			oss<< "[  ] ";
		else if(v.at(i)== 13)
			oss<< "[ *] ";
		else if(v.at(i)<10)
			oss<< "[ "<< v.at(i)<< "] ";
		else
			oss<< '['<<v.at(i)<< "] ";
		
	}
		
	oss<< "Stock Remaining: ";
	oss<< v.at(5);
	oss<<endl;
	return oss.str();
}
string buildingToString(std::vector<int> v){
	ostringstream oss;
	oss << "Building:";
	for (int i = 0; i < 4; i++)
	{
		if (v.at(i)<1)
			oss<< " [  ]";
		if (v.at(i)<10)
			oss<< " [ "<< v.at(i)<< ']';
		if (v.at(i)>9)
			oss<<" ["<< v.at(i)<<']';
	}
	oss<<endl;
	return oss.str();
}
void testBuildingToString(){
	std::vector<int> v1(4,7);
	std::vector<int> v2(v1);
	//cout<<"Building: [ 7] [ 7] [ 7] [ 7]\n";
	//cout<<"            1    2    3    4\n";

	assert(buildingToString(v1)=="Building: [ 7] [ 7] [ 7] [ 7]\n");
	assert(buildingToString(v2)=="Building: [ 7] [ 7] [ 7] [ 7]\n");
	assert(buildingToString(v1)==buildingToString(v2));
	std::vector<int> v3(4,10);
	assert(buildingToString(v3)=="Building: [10] [10] [10] [10]\n");
}

void testOtherPlayerToString(){
	std::vector<int> v1(6,6);
	std::vector<int> v2 (v1);
	string firststr="Discard: [ 6] [ 6] [ 6] [ 6]   Stock: [ 6] Stock Remaining: 6\n";
	cout<< firststr;
	cout<<otherPlayerToString(v1);
	cout<< "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --"<< endl;
	assert(otherPlayerToString(v1)== firststr);
	assert(otherPlayerToString(v1)==otherPlayerToString(v1));
	assert(otherPlayerToString(v1)==otherPlayerToString(v2));
	v2.at(1)=10;
	string secondstr="Discard: [ 6] [10] [ 6] [ 6]   Stock: [ 6] Stock Remaining: 6\n";
	assert(otherPlayerToString(v1)!=otherPlayerToString(v2));
	assert(otherPlayerToString(v2)==secondstr);
	std::vector<int> v3 (7,10);
	v3.at(2)=11;
	v3.at(6)=150;
	string thirdstr= "Discard: [10] [10] [11] [10]   Stock: [10] Stock Remaining: 10\n";
	assert(otherPlayerToString(v2)!=thirdstr);
	assert(otherPlayerToString(v3)==thirdstr);
	string fourthstr= "Discard: [10] [10] [ *] [10]   Stock: [ *] Stock Remaining: 19\n";
	std::vector<int> v4 (v3);
	v4.at(2)= 13;
	v4.at(4)= 13;
	v4.at(6)= 15;
	v4.at(5)=19;
	assert(otherPlayerToString(v4)==fourthstr);
}
string curPlayerToString(std::vector<int> v){
	ostringstream oss;
	oss<< "Hand: ";
	for (int i = 0; i < 10; i++)
	{
		if (i==5)
			oss<< " Discard: ";
		if(i==9)
			oss<< " Stock: ";
		if(v.at(i)==0)
			oss<< "[  ] ";
		else if(v.at(i)== 13)
			oss<< "[ *] ";
		else if(v.at(i)<10)
			oss<< "[ "<< v.at(i)<< "] ";
		else
			oss<< '['<<v.at(i)<< "] ";	
	}
	oss<<" Stock Remaining: ";
		oss<<v.at(10)<<endl;
	
	return oss.str();
}
void testCurPlayerToString(){
	std::vector<int> v1(11,0);
	v1.at(9)=13;
	v1.at(4)= 7;
	v1.at(10)= 30;
	cout<<curPlayerToString(v1);
	assert(curPlayerToString(v1)=="Hand: [  ] [  ] [  ] [  ] [ 7]  Discard: [  ] [  ] [  ] [  ]  Stock: [ *]  Stock Remaining: 30\n");
	std::vector<int> v2 (12);
	for (int i = 0; i < 12; ++i)
		v2.at(i)= i+2;
	assert(curPlayerToString(v2)=="Hand: [ 2] [ 3] [ 4] [ 5] [ 6]  Discard: [ 7] [ 8] [ 9] [10]  Stock: [11]  Stock Remaining: 12\n");
	v2.at(6)=0;
	v2.at(2)=12;
	assert(curPlayerToString(v2)=="Hand: [ 2] [ 3] [12] [ 5] [ 6]  Discard: [ 7] [  ] [ 9] [10]  Stock: [11]  Stock Remaining: 12\n");
	std::vector<int> v3 (v1);
	std::vector<int> v4 (v2);
	assert(curPlayerToString(v2)==curPlayerToString(v4));
	assert(curPlayerToString(v1)==curPlayerToString(v3));
	v2.at(10)= 5;
	assert(curPlayerToString(v2)=="Hand: [ 2] [ 3] [12] [ 5] [ 6]  Discard: [ 7] [  ] [ 9] [10]  Stock: [11]  Stock Remaining: 5\n");

}
void wholeTest(){
	string p1= "Alex";
	string p2= "Michael";
	string p3= "Isaac";
	string p4= "Andrew";
	string p5= "N/A";
	string p6= "N/A";
	string line;
	string s8;
	std::vector<int> v(37,5);
	std::vector<string> nameVect(4);
	nameVect.at(0)= p1;
	nameVect.at(1)= p2;
	nameVect.at(2)= p3;
	nameVect.at(3)= p4;
	ifstream tFile;
	tFile.open("PrintMenuTest.txt");
	ostringstream oss;
	for (int i = 0; i < 9; i++)
	{
		std::getline(tFile, line);
		oss<< line<< endl;
	}
	cout<< oss.str();
	cout<<createStr(nameVect, v);
	
	assert(oss.str()== createStr(nameVect, v));
	ostringstream oss2;
	std::vector<int> v2(37,10);
	v2.at(4)=13;
	v2.at(5)=5;
	v2.at(6)=5;
	v2.at(11)=9;
	v2.at(12)=5;
	v2.at(17)=13;
	v2.at(19)=5;
	v2.at(20)=8;
	v2.at(24)=8;
	v2.at(36)=0;
	v2.at(35)=5;
	cout<< createStr(nameVect, v2);
	for (int i = 0; i < 9; i++)
	{
		std::getline(tFile, line);
		oss2<< line<< endl;
	}
	cout<< oss2.str();


	assert(oss2.str()== createStr(nameVect, v2));


	tFile.close();

}

string createStr(std:: vector <string> nameVect, std:: vector <int> v){
	int players=4;
	int cur= 3;
	int place=0;
	unsigned long size=0;
	string underBuilding="            1    2    3    4\n";

	ostringstream oss;
	for (int i = 0; i < players; i++)
	{
		if (i!=(cur-1))
		{
			if (size<nameVect.at(i).length())
				size=nameVect.at(i).length();
			std::vector<int> otherV(7);
			for (int j = 0; j < 7; j++)
			{
				otherV.at(j)= v.at(place);
				place++;
			}
			oss<< nameVect.at(i)<< ":  ";
			oss<< otherPlayerToString(otherV);
		}
	}
	for (int i = 0; i < (int)(size+ 2)/3 +1 ; ++i)
		oss<< "-- ";
	oss<< "-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "<< endl;
	std::vector<int> bldg(4);
	for (int i = 0; i < 4; i++)
	{
		bldg.at(i)= v.at(place);
		place++;
	}
	oss<<buildingToString(bldg);
	oss<<underBuilding<<endl;
	std::vector<int> curPlayer(12);
	for (int i = 0; i < 12; i++)
	{
		curPlayer.at(i)= v.at(place);
		place++;
	}
	oss<< nameVect.at(cur-1)<< ": ";
	oss<<curPlayerToString(curPlayer);
	size=nameVect.at(cur-1).length();
	
	oss<<"               5    6    7    8    9             10   11   12   13           14\n";
	return oss.str();
}


