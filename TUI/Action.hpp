/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#ifndef _Action_H
#define _Action_H

class Action {
	private:
		int playerID;
		int oldLoc;
		int newLoc;

	public:
		Action(int ID, int a, int b): playerID(ID), oldLoc(a), newLoc(b) {};

		~Action() {}; //destructor

		//method for undoing moves
		void reverse() {
			
		};

		//check that card is moving from valid oldLoc to valid newLoc
		//***doesn't check if the pile it is coming from is empty or if the build pile is in order (if the card is being moved there)
		bool checkMovement() {
			if (oldLoc < 5 || newLoc<1)
				return false;
			if (newLoc > 13 || oldLoc > 14)
				return false;
			if(newLoc > 4 && newLoc < 10)
				return false;

			//check that player is not moving card from one discard pile to another discard pile
			if (oldLoc >= 10 && oldLoc <= 13 && newLoc >= 10 && newLoc <=13)
				return false;
			//if player is moving card from stock pile, check that it is moving to build pile
			if (oldLoc == 14 && newLoc > 4)
				return false;

			return true;
		}

		bool isDone() {
			//player is moving card from hand to discard pile
			if (oldLoc >= 5 && oldLoc <= 9 && newLoc >= 10 && newLoc <= 13)
				return true;
			else
				return false;
		}

		int getPlayerID() {
			return playerID;
		}

		int getBegin() {
			return oldLoc;
		}

		int getEnd() {
			return newLoc;
		}

		void setBegin(int x) {
			oldLoc = x;
		}

		void setEnd(int x) {
			newLoc = x;
		}
};

#endif
