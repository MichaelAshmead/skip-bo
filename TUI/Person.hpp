/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#ifndef _Person_H
#define _Person_H

#include "Game.hpp"
#include "Player.hpp"
#include "Action.hpp"
#include <vector>
#include <string>
#include <iterator>

using std::vector;
using std::string;
using std::cin;
using std::cout;

class Person : public Player {
 	public:
  		Person(int id, string na) : Player(id, na, vector<Pile>(), Pile(31), vector<Pile>()) {
			for (int x = 0; x < 4; x++) {
				DiscardPiles.push_back(Pile(162));
			}
			for (int x = 0; x < 5; x++) {
				HandPile.push_back(Pile(2));
			}
		};

		~Person() {
			//clear HandPile piles
			vector<Pile>::iterator it0;
			for (it0 = HandPile.begin(); it0 < HandPile.end(); it0++) {
				it0->~Pile();
			}

			//clear StockPile
			StockPile.~Pile();
			
			//clear discard piles
			vector<Pile>::iterator it;
			for (it = DiscardPiles.begin(); it < DiscardPiles.end(); it++) {
				it->~Pile();
			}
		};

		Action getAction(Game &G) {
			//show current board to Person Player
			G.displayBoard();

			vector<int> beginAndEnd = promptAction();
			if (beginAndEnd.size() == 1){
				G.saveGame();
				cout << "Game Saved." << endl << "Please enter your move." << endl;
				beginAndEnd = promptAction();
			}
			Action action(this->getPlayerID(), beginAndEnd.at(0), beginAndEnd.at(1));

			//make sure action is valid
			while (!G.isValid(action)) {
				cout << "Move is illegal. Please enter another move." << endl;
				beginAndEnd = promptAction();
				action.setBegin(beginAndEnd.at(0));
				action.setEnd(beginAndEnd.at(1));
			}
			return action;
		};

		//ask for user input
		vector<int> promptAction() {
			int firstInput;
			cout << "Begin? (Use 100 to Save):" << endl;
			cin >> firstInput;

			vector<int> beginAndEnd = vector<int>();
			if (firstInput==100) {
				beginAndEnd.push_back(100);
				return beginAndEnd;
			}
			int secondInput = 0;
			if (firstInput >= 2 && firstInput <= 14) {
				cout << "End?:" << endl;
				cin >> secondInput;
			}

			beginAndEnd.push_back(firstInput);
			beginAndEnd.push_back(secondInput);
			
			return beginAndEnd;
		};
}; 

#endif
