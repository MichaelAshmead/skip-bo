/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#ifndef _CARD_H
#define _CARD_H

class Card {  
	private:
		int cardID;
		int value; //Skip-Bo's temp value
	public:
	  	Card(int id = 0): cardID(id) {};
		
		~Card() {}; //destructor

	  	int getCardID() {
			return cardID;
		};

		int getValue() {
			return value;
		};

		bool operator==(Card C2) {
			return (this->getCardID() == C2.getCardID());
		};
};

#endif



