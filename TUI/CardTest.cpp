/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include "Card.hpp"
#include <cassert>
#include <iostream>
#include <string>

using std::cout;
using std::endl;

int main() {
	cout << "Testing Card Class..." << endl;

	//create a regular card
	Card card0(1);
	assert(card0.getCardID() == 1);
	Card card1(13);
	assert(card1.getCardID() == 13);

	assert(!(card0==card1));
	Card card2(13);
	assert(card1==card2);

	cout << "Card Class Passed All Tests!" << endl;
	return 0;
}
