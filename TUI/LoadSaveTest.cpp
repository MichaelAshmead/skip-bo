/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include "Game.hpp"
#include <cassert>

class LoadSaveTest
{
public:
	LoadSaveTest();
	~LoadSaveTest();

	/* data */
	static void Test(){
		
		string fileName= "Save.txt";
		Game G(fileName);
		assert(G.playerWon==-1);
		assert(G.numberPlayers==2);
		//cout<<"G Scrap Size:"<<G.scrap.getRealSize()<<endl;
		//G.displayBoard();
		//G.saveGame();
		string fid="asdf.txt";
		//Game G2(fid);
		//cout<<"G2 Scrap Size:"<<G2.scrap.getRealSize()<<endl;
		//G2.saveGame();
		/*for (int i = 0; i < G.scrap.getRealSize(); i++){
			cout<< G.scrap.getCard(i).getCardID()<< " ";
		}
		cout<<endl;*/

	};
};

int main(void){
	LoadSaveTest::Test();
	//LoadSaveTest::SaveTest();
	return 0;
}
