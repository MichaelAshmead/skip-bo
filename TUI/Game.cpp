/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include "Game.hpp"
#include "printHelper.cpp"

#include <vector>
#include <iostream>
#include <algorithm> //used for shuffling deck of cards
#include <ctime>
#include <cstdlib>
#include <fstream>

using std::cout;
using std::endl;
	
Game::Game(int numberPlaying, vector<string> names, vector<int> typeOfPlaying, int stockSize, int random) {
	numberPlayers = numberPlaying;
	playerNames = names;
	stockPileSize = stockSize;
	HistIter=0;

	//create 4 building piles of 162 maxsize
	for (int x = 0; x < 4; x++) {
		buildingPiles.push_back(Pile(162));
	}

	//initialize players
	for (int x = 1; x <= numberPlaying; x++) {
		StockRemaining.push_back(stockSize);
		AIs.push_back(typeOfPlaying.at(x-1));
		if (typeOfPlaying.at(x - 1) == 0) { //initialize a Person Player
			players.push_back(new Person(x-1, names.at(x - 1)));
		}
		else { //initialize an AI Player
			players.push_back(new AI(x-1, names.at(x - 1)));
		}
	}

	vector<Card> deck = vector<Card>();
	//create a deck of cards
	for (int x = 1; x <= 12; x++) {
		for (int y = 1; y <= 12; y++) {
			deck.push_back(Card(y));
		}
	}

	//add Skip-Bo cards
	for (int x = 1; x <= 18; x++) {
		deck.push_back(Card(13));
	}

	//shuffle cards
	if (random == 0)
		srand(time(NULL));
	else
		srand(random);
	std::random_shuffle(deck.begin(), deck.end());

	for (int i = 0; i < numberPlaying; i++){	
		players.at(i)->StockPile.addCard(0);
	}


	//put the cards into each player's stock pile
	for (int x = 0; x < numberPlaying; x++) {
		for (int y = 0; y < stockSize; y++) {
			players.at(x)->StockPile.addCard(deck.back().getCardID()); //add the last card to the player's stockpile
			deck.pop_back(); //remove the last card from the deck
		}
	}
	draw.addCard(0);
	scrap.addCard(0);
	//put the rest of the cards into the draw pile
	while (!deck.empty()) {
    		draw.addCard(deck.back().getCardID());
   		deck.pop_back();
  	}

	//initialize placeholder cards with values of 0 in the build piles
	for (int x = 0; x < 4; x++) {
		buildingPiles.at(x).addCard(0);
	}

	//initialize placeholder cards with values of 0 in the handpiles and discard piles of each player
	for (int x = 0; x < numberPlaying; x++) {
		for (int y = 0; y < 4; y++) {
			players.at(x)->DiscardPiles.at(y).addCard(0);
		}
		for (int z = 0; z < 5; z++) {
			players.at(x)->HandPile.at(z).addCard(0);
		}
	}
}

//this method has the while loop that loops through every player and allows each player to do their turn
void Game::playGame() {
	bool stillUp = true;
	while (playerWon == -1) {
		//fill current player's hand at the beginning of their turn
		for (int x = 0; x < 5; x++) {
			if (players.at(currentPlayer)->HandPile.at(x).getBack().getCardID() == 0){
				Action drawToHand = Action(currentPlayer, 0, x + 5);
				doAction(drawToHand);
			}
		}

		while (stillUp) {
			//fill current player's hand if the player plays all their cards
			if (players.at(currentPlayer)->emptyHand()){
				for (int x = 0; x < 5; x++) {
					Action drawToHand = Action(currentPlayer, 0, x + 5);
					doAction(drawToHand);
				}
			}

			//get Player's valid action
			Action playerMove = players.at(currentPlayer)->getAction(*this);
				
			//execute the action
			doAction(playerMove);
			//if the move reduces the amount of stock cards in the pile, then reduce the cooresponding 
			//integer for the StockRemaining vector
			if (playerMove.getBegin()==14){	
				int temporary=StockRemaining.at(playerMove.getPlayerID());
				StockRemaining.at(playerMove.getPlayerID())=temporary-1;
			}

			//check if the player's turn is over or not
			stillUp = !playerMove.isDone();

			//after each action, check if player has won
			if (players.at(currentPlayer)->StockPile.getSize() == 0) {
				playerWon = currentPlayer;
				break;
			}
		}
		stillUp = true;
		currentPlayer++;
		if (currentPlayer == numberPlayers)
			currentPlayer = 0;
	}

	//somebody won the game so output the winner's name
	cout << players.at(playerWon)->getName() << " is the winner!" << endl;
}

bool Game::isValid(Action a) {
	/*
	How to determine if move is valid:
	-- Begin
		-- must be from own player's discard, stock, or hand pile
		-- ^above pile that card is coming from can't be empty
	-- End
		-- must be build or discard pile
		-- ^build must be in order
	*/
	
	//not a valid start and end position
	if (a.checkMovement() == false)
		return false;
	//if card is moving to build pile, check that the new card is one greater than previous
	if (a.getEnd() >= 1 && a.getEnd() <= 4) {
		//if building pile has something in it, the moving card should be 1 value greater or a Skip-Bo
		//check if the card at the end of the building is a Skip-Bo itself
			bool Skip = true;
			int skipCounter = 1;
		 	int iter = buildingPiles.at(a.getEnd() - 1).getSize();
		 	while (iter != 0 && Skip) {
		 		if (buildingPiles.at(a.getEnd() - 1).getCard(iter).getCardID() != 13)
		 			Skip = false;
		 		if (Skip) {
		 			skipCounter++;
					iter--;
				}
			}
			if (getMovingCardID(a) != buildingPiles.at(a.getEnd() - 1).getCard(iter).getCardID()+skipCounter && getMovingCardID(a) != 13)
				return false;
	}
	//check that HandPile card is not empty at specified begin
	else if ((a.getBegin() >= 5 && a.getBegin() <= 9) && players.at(currentPlayer)->HandPile.at(a.getBegin() - 5).getBack().getCardID() == 0) {
		return false;
	}
	//check that discard pile is not empty at specified begin
	else if ((a.getBegin() >= 10 && a.getBegin() <= 13) && players.at(currentPlayer)->DiscardPiles.at(a.getBegin() - 10).getBack().getCardID() == 0) {
		return false;
	}
	
	return true;
}

//returns card's cardID in the given action
int Game::getMovingCardID(Action a) {	
	//if card is in draw pile
	if (a.getBegin() == 0)
		return draw.getBack().getCardID();

	//if card is in building pile
	else if (a.getBegin() <= 4)
		return buildingPiles.at(currentPlayer).getBack().getCardID();

	//if card is in hand pile
	else if (a.getBegin() <= 9)
		return players.at(currentPlayer)->HandPile.at(a.getBegin() - 5).getBack().getCardID();

	//if card is in discard pile
	else if (a.getBegin() <= 13)
		return players.at(currentPlayer)->DiscardPiles.at(a.getBegin() - 10).getBack().getCardID();

	//if card is in stock pile
	else if (a.getBegin() == 14)
		return players.at(currentPlayer)->StockPile.getBack().getCardID();

	//if card is in scrap pile
	else
		return scrap.getBack().getCardID();
}

void Game::doAction(Action a) {
	//temporarily store value of card
	int temp = getMovingCardID(a);

	//***remove the card from begin pile***
	//if card is in draw pile
	if (a.getBegin() == 0) {
		//check if draw pile is empty
		//if draw pile is empty, shuffle cards from scrap pile and add to draw pile
		if (draw.getSize() == 0) {
			//shuffle scrap pile
			scrap.removeCard(0);
			scrap.shuffle();
			int temp= scrap.getCard(0).getCardID();
			scrap.setLoc(0,0);
			scrap.addCard(temp);

			//put the cards into the draw pile
			while (scrap.getSize() != 0) {
    				draw.addCard(scrap.getBack().getCardID());
   				scrap.removeCard(scrap.getBack().getCardID());
  			}
		}

		draw.removeCard(temp);
	}

	//if card is in building pile
	if (a.getBegin() >= 1 && a.getBegin() <= 4)
		buildingPiles.at(a.getBegin() - 1).removeCard(temp);

	//if card is in hand pile
	if (a.getBegin() >= 5 && a.getBegin() <= 9)
		players.at(currentPlayer)->HandPile.at(a.getBegin() - 5).removeCard(temp);

	//if card is in discard pile
	if (a.getBegin() >= 10 && a.getBegin() <= 13)
		players.at(currentPlayer)->DiscardPiles.at(a.getBegin() - 10).removeCard(temp);

	//if card is in stock pile
	if (a.getBegin() == 14)
		players.at(currentPlayer)->StockPile.removeCard(temp);

	//if card is in scrap pile
	if (a.getBegin() == 15)
		scrap.removeCard(temp);

	//***adding the card to end pile***
	if (a.getEnd() == 0)
		draw.addCard(temp);
	//if card will be in building pile
	if (a.getEnd() >= 1 && a.getEnd() <= 4) {
		buildingPiles.at(a.getEnd() - 1).addCard(temp);

		//check if building pile is filled up with 12 cards
		if (buildingPiles.at(a.getEnd() - 1).getSize() == 12) {
			//move cards to scrap pile
			for (int x = 1; x <= 12; x++) {
				//temporarily store card value from build pile
				int tempCardID = buildingPiles.at(a.getEnd() - 1).getBack().getCardID();
				//remove card from build pile
				buildingPiles.at(a.getEnd() - 1).removeCard(tempCardID);
				//add card to scrap pile
				scrap.addCard(tempCardID);
			}
		}
	}

	//if card will be in hand pile
	if (a.getEnd() >= 5 && a.getEnd() <= 9)
		players.at(currentPlayer)->HandPile.at(a.getEnd() - 5).addCard(temp);

	//if card will be in discard pile
	if (a.getEnd() >= 10 && a.getEnd() <= 13)
		players.at(currentPlayer)->DiscardPiles.at(a.getEnd() - 10).addCard(temp);

	//if card will be in stock pile
	if (a.getEnd() == 14)
		players.at(currentPlayer)->StockPile.addCard(temp);

	//if card will be in scrap pile
	if (a.getEnd() == 15)
		scrap.addCard(temp);
}

void Game::displayBoard() {
	vector <int> v;

	v.push_back(numberPlayers);
	v.push_back(currentPlayer);

	for (int i = 0; i < numberPlayers; i++){
		if (i != currentPlayer){
			for (int j = 0; j < 4; j++)
				v.push_back(players.at(i)->DiscardPiles.at(j).getBack().getCardID());
			v.push_back(players.at(i)->StockPile.getBack().getCardID());
			v.push_back(players.at(i)->StockPile.getSize());
		}
	}
	//find out whats in the building pile
	for (int i = 0; i < 4; i++){
		bool Skip = true;
		int skipCounter = 0;
		int iter = buildingPiles.at(i).getSize();
		while (iter != 0 && Skip) {
		 	if (buildingPiles.at(i).getCard(iter).getCardID() != 13)
		 		Skip = false;
		 	if (Skip) {
		 		skipCounter++;
				iter--;
			}
		}

		v.push_back(buildingPiles.at(i).getCard(iter).getCardID() + skipCounter);
	}
	for (int i = 0; i < 5; i++)
		v.push_back(players.at(currentPlayer)->HandPile.at(i).getBack().getCardID());
	for (int j = 0; j < 4; j++)
		v.push_back(players.at(currentPlayer)->DiscardPiles.at(j).getBack().getCardID());
	v.push_back(players.at(currentPlayer)->StockPile.getBack().getCardID());
	v.push_back(players.at(currentPlayer)->StockPile.getSize());

	cout << createStr(playerNames, v);
}
void Game::saveGame() {
	string fid;
	vector<int> Arrangement;	
	cout << "Saving Game to File:" << endl;
	cout << "Please type in the name of the file to save to:" << endl;
	cin >> fid;
	std::ofstream gameFile;
	gameFile.open(fid);
	gameFile << numberPlayers << ' ';
	gameFile << currentPlayer << ' ';
	gameFile << stockPileSize << ' ';
	gameFile << HistIter;
	gameFile << endl;
	for (int i = 0; i < numberPlayers; i++)
		gameFile << AIs.at(i) << ' ';
	gameFile << endl;
	for (int i = 0; i < numberPlayers; i++)
		gameFile << playerNames.at(i) << ' ';
	gameFile << endl;

	//saves the building data
	for (int i = 0; i < 4; ++i) {
		int count = 0;
		while (count < buildingPiles.at(i).getRealSize()) {
			gameFile << buildingPiles.at(i).getCard(count).getCardID() << ' ';
			count++;
		}
	}
	gameFile << endl;
	int counter = 0;
	for(int i = 0; i < numberPlayers; i++){//saves the player data
		counter = 0;
		for (int h = 0; h < 5; h++){
			counter = 0;
			while (counter<players.at(i)->HandPile.at(h).getRealSize()) {
				gameFile << players.at(i)->HandPile.at(h).getCard(counter).getCardID() << ' ';
				counter++;
			}
			if (players.at(i)->HandPile.at(h).getRealSize() == 0)
				gameFile << 0 << ' ';
		}
		gameFile << endl;
		for (int d = 0; d < 4; d++){
			counter = 0;
			while (counter < players.at(i)->DiscardPiles.at(d).getRealSize()) {
				gameFile << players.at(i)->DiscardPiles.at(d).getCard(counter).getCardID() << ' ';
				counter++;
			}
			if (players.at(i)->DiscardPiles.at(d).getRealSize() == 0)
				gameFile << 0 << ' ';
		}
		gameFile << endl;
		counter = 0;
		if (players.at(i)->StockPile.getRealSize() == 0)
			gameFile << 0 << ' ';
		else if (players.at(i)->StockPile.getCard(0).getCardID() != 0)
			gameFile << 0 << ' ';
		while (counter < players.at(i)->StockPile.getRealSize()){
			gameFile << players.at(i)->StockPile.getCard(counter).getCardID() << ' ';
			counter++;
		}
		
		gameFile <<endl;
	}
	counter = 0;

	while (counter < draw.getRealSize()) {
		gameFile << draw.getCard(counter).getCardID() << ' ';
		counter++;
	}
	if (draw.getRealSize() == 0)
		gameFile << 0 << ' ';
	gameFile << endl;
	counter = 0;
	while (counter < scrap.getRealSize()) {
		gameFile << scrap.getCard(counter).getCardID() << ' ';
		counter++;
	}
	if (scrap.getRealSize() == 0)
		gameFile << 0 << ' ';
	cout << counter << endl;

	gameFile.close();
}

//constructor for loading a saved game from file
Game::Game(string fid) {
	vector<int> Arrangement;
	std::ifstream gameFile;
	int temp;
	string playerline;
	gameFile.open(fid);

	gameFile >> numberPlayers;
	gameFile >> currentPlayer;
	gameFile >> stockPileSize; 
	gameFile >> HistIter;

	for (int i = 0; i < numberPlayers; i++){
		gameFile >> temp;
		AIs.push_back(temp);
	}

	for (int i = 0; i < numberPlayers; i++) {
		gameFile >> playerline;
		playerNames.push_back(playerline);
	}
		
	int count = 168 + 10 * numberPlayers; //account for each 0 termination on each pile plus the 162 cards total
	for (int i = 0; i < count; i++) { //initializing the Arrangement vector
		gameFile >> temp;
		Arrangement.push_back(temp);
	}

	for (int x = 0; x < numberPlayers; x++) {
		if (AIs.at(x) == 0) { //initialize a Person Player
			players.push_back(new Person(x, playerNames.at(x)));
		}
		else { //initialize an AI Player
			players.push_back(new AI(x, playerNames.at(x)));
		}	
	}

	setArrangement(Arrangement);

	gameFile.close();
}


void Game::setArrangement(vector<int> v) {
	//each while loop goes from the start 0 of the current pile/ pile vector to the start 0 of the next pile/pile vector
	int b = -1;
	int place = 0;
	cout << v.size() <<endl;
	//this while loop accounts for the buildings
	for (int x = 0; x < 4; x++)
		buildingPiles.push_back(Pile(13));
	while (b < 4) {
		if (v.at(place) == 0)
			b++;	
		if (b < 4) {
			buildingPiles.at(b).addCard(v.at(place));
			place++;
		}
	}
	for (int i = 0; i < numberPlayers; i++){
		//Initialize the Hand for each Player
		int h = 0;
		int d = -1;
		int s = -1;
		for (int j = 0; j < 5; j++){
			players.at(i)->HandPile.push_back(Pile(2));
		}
		while (h < 5) { //counts through each handpile
			if (players.at(i)->HandPile.at(h).getRealSize() == 0){
				players.at(i)->HandPile.at(h).addCard(v.at(place));
				place++;
			}
			else if (v.at(place) != 0){
				players.at(i)->HandPile.at(h).addCard(v.at(place));
				place++;
				h++;
			}
			else
				h++;
		}
		for (int j = 0; j < 4; j++)
			players.at(i)->DiscardPiles.push_back(Pile(162));
		while (d < 4) { //counts through each handpile
			if (v.at(place) == 0)
				d++;
			if (d < 4) {
				players.at(i)->DiscardPiles.at(d).addCard(v.at(place));
				place++;
			}
		}

		//initialize the stock pile for each player
		while (s < 1) { //counts through each handpile
			if (v.at(place) == 0)
				s++;
			if (s < 1) {
				players.at(i)->StockPile.addCard(v.at(place));
				place++;
			}
		}
		StockRemaining.push_back(players.at(i)->StockPile.getSize());
	}
	//Initialize the Deal Pile
	draw = Pile(162);
	int marker = place + 1;
	while (v.at(marker) != 0)
		marker++;
	while (place < marker){ //counts through each handpile
		draw.addCard(v.at(place));
		place++;
	}
	
	//Initialize the scrap pile
	while (place<(int) v.size()) {
		scrap.addCard(v.at(place));
		cout << v.at(place) << " ";
		place++;
	}
	cout << endl;
}
