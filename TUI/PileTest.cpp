/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include "Pile.hpp" //Pile.hpp includes Card.hpp!
#include <cassert>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::vector;

int main(void) {
//PILE 1 TEST
  Card c1(3);
  Card c2(4);
  Card c3(13);
  Card c4(13);
  Card c5(2);

  Pile hand(5);
  hand.addCard(3);
  hand.addCard(4);
  hand.addCard(13);
  hand.addCard(13);
  hand.addCard(2);
  assert(hand.getSize()==5);
  Card c10(6);
  assert(!(hand.addCard(6)));
  
  assert(hand.removeCard(3));
  
  assert(!(hand.removeCard(12)));
  
  assert(hand.removeCard(13));
  
  assert(hand.removeCard(13));
  
  assert(hand.removeCard(4));
  assert(hand.removeCard(2));
  assert(!(hand.removeCard(5))); 
  assert(hand.getSize()!=5);


//PILE 2 TEST
  Card c6(2);
  Card c7(6);
  Card c8(10);
  Card c9(13);
  
  Pile discard1(4);
  discard1.addCard(2);
  discard1.addCard(6);
  discard1.addCard(10);
  discard1.addCard(13);
  assert(discard1.getSize()==4);
  Card c11(12);
  assert(!(discard1.addCard(12)));
  assert(discard1.removeCard(2));
  assert(discard1.removeCard(6));
  assert(discard1.removeCard(10));
  assert(discard1.removeCard(13));
  assert(!(discard1.removeCard(2)));
  assert(!(discard1.removeCard(13)));
  assert(discard1.getSize()!=4);

}
