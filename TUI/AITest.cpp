/*
600.120 (3-7)
Final Project

Andrew Colombo	        acolomb2	acolomb2@jhu.edu	267 226 2343
Michael Ashmead		mashmea1	mashmea1@jhu.edu	484 682 9355
Alex Owen		aowen10 	aowen10@jhu.edu 	704 661 8270
Isaac Nemzer		inemzer 	inemzer@jhu.edu	        310 779 0039
*/

#include "AI.hpp"
#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;

int main() {
	cout << "Testing AI Class..." << endl;

	//Game G(2, {"AI1", "Andrew"}, {0,1}, 20);
	
	AI AI1(0, "AI1");
	assert(AI1.getPlayerID() == 0);
	//assert(AI1.prefDiscard(G)<5 && AI1.prefDiscard(G)>0);
	//assert(AI1.getBegin() > 0 && AI1.getBegin() < 15);
	//assert(AI1.getEnd() > 0 && AI1.getEnd() < 15);
	
	AI AI2(2, "AI1");
	assert(AI2.getPlayerID() == 2);
	//assert(AI2.getBegin() > 0 && AI2.getBegin() < 15);
	//assert(AI2.getEnd() > 0 && AI2.getEnd() < 15);

	AI AI3(4, "AI1");
	assert(AI3.getPlayerID() == 4);
	//assert(AI3.getBegin() > 0 && AI3.getBegin() < 15);
	//assert(AI3.getEnd() > 0 && AI3.getEnd() < 15);

	AI AI4(6, "AI1");
	assert(AI4.getPlayerID() == 6);
	//assert(AI4.getBegin() > 0 && AI4.getBegin() < 15);
	//assert(AI4.getEnd() > 0 && AI4.getEnd() < 15);

	cout << "AI Class Passed All Tests!" << endl;
	
	return 0;
}
